---
title: "About"
lastmod: 2021-04-14T14:50:47-03:00
draft: false
---

## Hi!

I'm Kat, and I like Maths and Computers. My main broad areas of mathematical interest are Algebraic Topology, Abstract Algebra, and Category Theory. Currently pretending to be a Cloud Engineer.

Some things I like:

-   The GNU/Linux world: I love everything about it, and I've been a proud Linux user ever since I was introduced to it back when I was a child. One of my dreams is to get involved in big Open-Source projects and give back to the community :purple_heart:.

- Languages: I love languages! I studied French for most of my school years, and yet I can't remember a thing! However I grew up speaking English and Spanish. Currently learning German by myself. Some time ago I also studied Italian for a year and a half and a bit of Japanese on the side.

- Music and videogames: I play the guitar and often post cringy covers. I also may or may not have a few hundred games in my Steam library. 

- Skateboarding and basketball. 

- Cats.


