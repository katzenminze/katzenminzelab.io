---
title: "My Linux Distrovery"
date: 2021-04-16T08:20:47-03:00
tags: ["linux", "arch", "distros"]
draft: false
autoCollapseToc:  true
slug: "my-linux-distrovery"
---
In this _official first post_ I'd like to give an overview of the distros I've used in the past and my current one.

Let's begin with a bit of ancient history:

## Veridis Tro {#veridis-truo}

My Linux journey began the day I was introduced to Ubuntu by one of my father's coworkers, who was secretly as Linux nerd. This happened around the year 2004.


###  Debian After All {#debian-after-all}
<p style="text-align: center; font-size: 30px";>
{{<icon "debian">}}
</p>

As the edgy person I ~~am~~ was, I decided to install Debian by myself instead[^fn:1].
Certainly not a bright idea, since back in the day Debian was not a painless experience as it might be today. The installation process was too difficult, and maintaining the system was not something a total noob like myself could handle.
Despite the issues and several reinstalls, I managed to stick to it for nearly a year and a half.


### Easier, Better, Faster, Smaller {#easier-better-faster-smaller}

Prior to eventually admitting I wasn't ready to handle Debian, I used several lightweight distros that _ran from RAM_, as means of testing new things and also due to my hardware at that time being extremely limited in resources. In comparison to the experience with Debian, this distros felt much more manageable.

The ones I remember using the most are

-   Puppy Linux: I used this one the most. It was fun and different.
-   Slax: Felt very much like my Debian install (it's based on it!).
-   Knoppix: For like 30 minutes.
-   Damn Small Linux: Because it's so damn smol it had to be tested.

Eventually I got my hands on a better computer and at this point, running from RAM was not necessary. 


### Ubuntu Distro Club {#ubuntu-distro-club}

<p style="text-align: center; font-size: 30px";>
{{<icon "ubuntu">}}
</p>

 Installing and maintaining Ubuntu was much simpler of course. First I went with the default Gnome version, and having been using it for quite some time already and learning the installation process, I decided to do what every Linux user does (or should do!) which is to try and convince everyone around them to switch to Linux.
 Of course I volunteered for installing and maintaining the system to those willing to switch, and I even recall carrying around my Ubuntu/Xubuntu/Lubuntu[^fn:2] CDs and feeling like some kind of warrior fighting against the _unspeakable_ evils of the _unnameable_ spyware suite (**Hint**: starts with a W). I must say, the usual:

 > "There are no viruses"

 was often enough to convince most of the people, and for the stubborn I had to resort to a list of more thought-out arguments like:

-   For real, there are no viruses.
-   Did I mention it's free?.
-   No really, **there are no viruses**.

In the end, Ubuntu was a nice experience and I certainly learned a lot from using it. However, the Ubuntu era had to come to an end for me in 2010 when Unity was introduced. I absolutely hated Unity the moment it came out (and still do!) and at the time it also felt like the perfect excuse to go and try a different distro family.


### Get Hatty {#get-hatty}

<p style="text-align: center; font-size: 30px";>
{{<icon "fedora">}}
</p>


<p style="text-align: center";>
{{< figure src="https://media.giphy.com/media/gW9h5P8yReZgc/giphy.gif" >}}
</p>

My next distro was Fedora and I must say, it was a really enjoyable smooth experience. With a clean and elegant looking system it certainly added to the feeling of being one of the cool kids running a different distro.

Ultimately, my experience with Fedora was a brief one, and the reasons why I stopped using it was mostly because of dying hardware but also because I really missed the ease of installation when you have `.deb` packages at your disposal as well as a package manager with mainstream software.


### Mint Love {#mint-love}

<p style="text-align: center; font-size: 30px";>
{{<icon "linuxmint">}}
</p>

My next hardware was a laptop and my new distro was Linux Mint. It felt nice and intuitive from the start and the fact that it looked as pretty as it did (Cinnamon) certainly added to the feeling of being one of the average Linux users running a popular distro. I was nonetheless happy with that. Shortly after I got my hands on actual desktop hardware and decided to build a decent PC like the ones da real gamerz have.[^fn:3]
Of course the distro of choice for said new PC was Linux Mint, but I came across a huge issue many people have which is: many of my games didn't have Linux support. Evil deeds were performed in order to be able to play videogames, and that means resorting to dualbooting with the _unnameable_ spyware suite just for games.[^fn:4] It gets better in the end I promise.

Using Mint was a lovely experience and I certainly would recommend it to new users.

Despite not really needing a change, the distro hopper in me thought a change would be nice. I even started to notice the limitations of Mint in regards of being so tied up to specific sofware (like desktop environments) and decided I was ready for something powerful. 


### Something about Manjaro {#something-about-manjaro}

<p style="text-align: center; font-size: 30px";>
{{<icon "manjaro">}}
</p>

The search for the ultimate distro (for me) lead to Manjaro Linux. Having read about the wonders of the package manager _pacman_ and the [AUR](https://wiki.archlinux.org/index.php/Arch%5FUser%5FRepository) I felt ready to switch.

The time I spent with Manjaro was indeed short. However, it was well worth it since I could finally devote some time to ~~going full hipster~~ really learn how to setup a window manager instead of a desktop environment, a thing I was trying to do since my first year with Mint, and most importantly, it showed me what that something about Manjaro I liked really was. I was ready to commit.


## Archidynamic {#archidynamic}

[Btw, I use Arch.]^(I'm sorry for this)

Now that that's out of _my system_, yes, my distro hopping years ended in 2017 when I arrived at what feels like home.

Arch has been, a very rewarding experience. The fact that it comes with a minimal set of packages by default (compared to most popular distros at least), pacman, the AUR and the fact that it is fully customizable makes it my distribution of choice. It is true that the installation process is non-trivial for someone not used to working in the terminal (which I wasn't), but it's a matter of following instructions from the wiki, and you end up learning a ton along the way.

Contrary to popular belief, Arch has been a solid and stable experience and to this day it has yet to fail me. Being a rolling release certainly adds to the feeling of being one of the cool kids that ran a cool distro and bleeding-edge software. 

<p style="text-align: center;">
{{< icon "archlinux" >}} = :blue_heart:
<p>


### Oh Yeah {#oh-yeah}

During my time as an Arch user some important things happened like

-   In 2018 Valve released their compatibility layer called [Proton](https://github.com/ValveSoftware/Proton). This is wonderful news and what will lead to the eventual final blow[^fn:5] to the tyrannical reign of the _unnameable_. From the moment this was announced I decided to ditch my dualbooting setup and I went back to being a Linux Exclusive User. 
-   I learned how to exit vim.

I'll leave with a pic of my current setup

{{< figure src="arch.png" >}}




## Instant Crash

<p style="text-align: center; font-size: 30px";>
{{<icon "gentoo">}}
</p>

I experimented with Gentoo on my laptop. The installation process was very rewarding and I certainly learned a lot from it. However, the compilation times with my hardware are an extreme inconvenience, which led me back to Arch. I might try getting back to Gentoo in the future for my main Desktop PC if I have the time.


[^fn:1]: Okay, maybe it was just because of the fact that Debian releases were named after Toy Story characters, which I was a huge fan of.
[^fn:2]: Yes, no Kubuntu since I've never been a fan of KDE.
[^fn:3]: This moment coincides with me ditching the videogame console ~~peasantry~~ world.
[^fn:4]: I swear to Saint iGNUcius I only used it for gaming, and it felt wrong the entire time.
[^fn:5]: As of today, pretty much all of the games I've tested run out of the box or with minimal intervention.

<p style="text-align: center; font-size: 50px";>
{{<icon "comment">}}
</p>
{{< comments my-linux-distrovery >}}
