---
title: "Any Colour U Like"
date: 2023-08-17T14:27:02-03:00
draft: false
tags: ["psychedelics", "mathematics", "mental-health", "philosophy" ]
---
{{< badge >}}
New article!
{{< /badge >}}
{{< lead >}}
 “The urge to transcend self-conscious selfhood is a principal appetite of the soul.”

― Aldous Huxley, The Doors of Perception
{{< /lead >}}

Today I'd like to share some thoughts and info on a topic I find quite intriguing and the seemingly apparent relationship it has with nature and our minds.

{{< alert  >}}
**Disclaimer: Educational Intent Only**
{{< /alert >}}

> _This post is meant solely for educational purposes to provide insights into the psychedelic subjective experience. It does not advocate or endorse the use of any substances. Readers are encouraged to make informed decisions, abide by the law and avoid substance use._

## Perception In Perspective
{{< figure default=true src=grass.jpg caption="Grass by Chelsea Morgan" >}}

By the nature of it, our ``perception`` of the world and the self conditions us to a very subjective experience of what we call our ``reality``. It is safe to say that, despite the accounts of some, the nature of an objective reality is something we just don't know for sure, and as such, our intuitive and conceptual understanding of the world seems to be no more than an abstraction. Things may objectively _be_, but we assign them a concept, a word, a mental image... all happening in the brain.

However, existential crises aside, our perception of the self and the world seem to suggest that there is indeed, something else out there from which we derive experiences. And we're happy to call this the _real world_ 💫.

** {{< icon "music" >}} Space Odyssey theme starts playing {{< icon "music" >}}**
<iframe style="border-radius:12px" src="https://open.spotify.com/embed/track/43YwOmGUOS3zzGvj1Feszb?utm_source=generator&theme=0" width="100%" height="100" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Following on the topic of perception, our human condition and the way the brain operates dictate how the world is presented to us. An abstract ``inner world`` ruled by our neurochemistry is where in principle concepts such as color, sound, shape and whatnot exist for us. All derived seemingly from our interactions with the ``outer world``. But how separate are this inner and outer realms?

## Mind Manifesting 
<!---<div style='position:relative; padding-bottom:calc(56.25% + 44px)'><iframe src='https://gfycat.com/ifr/BowedLinedBunting' frameborder='0' scrolling='no' width='100%' height='100%' style='position:absolute;top:0;left:0;' allowfullscreen></iframe></div>
--->
From the Greek **ψυχή** (psyche “mind or soul”) and  **δῆλος** (delos, “manifest, visible”), Psychedelics can open a door to another realms of perception. The experience in itself can be very transformative and shake the very foundations of our reality and the relationship we have with the world around us.

## The Brain
The mechanism of action of psychedelics in the brain is highly complex, and knowing how they work and do what the do implies having full knowledge of how the brain works, which is safe to say we know very little about. Some studies have shown[^fn:1] that during such experiences the brain's default mode network is afk, while other parts of the brain that don't normally interact, start to DM each other. 

As a side note, I think one of the most intriguing details about our physiology is the presence of naturally occurring psychedelics produced within the brain and found throughout many plants and animals[^fn:2]. It is speculated that they may play a role in our "world building" and have a very significant relationship with near-death experiences and the supposed interactions with seemingly external entities[^fn:3]. 


{{< figure src="https://royalsocietypublishing.org/cms/asset/b2dd8991-eb92-4885-9bf2-ea9db536565d/rsif20140873f06.jpg" caption="a) Normal  b) Tripping balls">}}

>The figure above represents regions of the brain linked by neurological pathways before and during a psychedelic experience.

## The Visuals

This increased interconnectedness can perhaps help us explain some of the more striking qualities of the experience, like for example the often familiar visual aspect. This subjective effects in a way can be interpreted as a very intimate and visual representation of the neurological basis of our minds where thoughts and feelings can manifest and merge visually with the environment by how the new neurological pathways carry information directly to the visual cortex.

An in depth description of visuals can be found in the following video by Josie Kins. For a more comprehensive version refer to the [Subjective Effect Index](https://effectindex.com/):
<iframe width="560" height="315" src="https://www.youtube.com/embed/DWWvl5dSmEs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Even though the experience is quite personal, common archetypes such as the encompassing and distinctive geometry seem to be something quite prevalent and well structured.

### Psychedelic Geometry
{{< figure src="https://www.alexgrey.com/art-images/Collective-Vision-1995-Alex-Grey-watermarked.jpg" caption="Collective Vision by Alex Grey" >}}
Vivid visual patterns seem to be deeply rooted in our brains and present throughout almost all cultures. By a series of experiments that began in the 1920s, biological psychologist Heinrich Klüver took upon himself the job of classifying visual hallucinatory patterns into four main broad categories which he called ``form constants``

{{< figure default=true src="form_constants.png" caption="a) Funnels b) Spirals c) Honeycombs d) Cobwebs" >}}

**For the math nerds**: Such imagery has been shown to originate in the first area of the visual cortex known as V1. V1 can be modelled as an Euclidian plane of condensed neurological pathways, where each point of the V1 "sheet" can be mathematically mapped to the polar coordinates that define a point in our field of view. This is called a retino-cortical map. 

Overly simplifying, turns out the form constants of funnels, spirals and circles, correspond in such map to stripes or rays of neural activity in V1 where the angle varies, while the honeycomb or chess-board patterns correspond to hexagonal activity in V1. This is quite interesting as rays and hexagons are known to be present in instances of pattern formation models and it seems to be analogous to Turing's reaction-diffusion patterning process[^fn:4].

# The Ego
The complexity of the psychedelic experience is impossible to fully describe, and as mentioned, the profoundness and spiritual aspect of it can lead to a wider perspective on consciousness and the world. I'd like to talk a bit more in this regard about the ``ego`` and the notion of selfhood. 

## The Self Goes Away
<!---<div style='position:relative; padding-bottom:calc(66.67% + 44px)'><iframe src='https://gfycat.com/ifr/PeskyLavishGerenuk' frameborder='0' scrolling='no' width='100%' height='100%' style='position:absolute;top:0;left:0;' allowfullscreen></iframe></div>
--->
Another of the key aspects of the experience is the dissolution of the self and the momentary rupture of that separation between the observer and the object. The conscious mind infers the properties of self-awareness just as it infers the properties of objects, within a physical and psychological frame. In this regard, selfhood seems to be no more than an useful representation of a more general and enduring "mind substance" in which cognition takes place. As such one could argue that the self lacks the quality of being an entity by itself[^fn:5]. And what could this imply? 

The dissolution of the ego sort of leads us to a poetic way of interpreting such an entity comprised of pure "selfless" perception as being ``One With The World.``  

## Mental Health
Recent interest in the implications of psychedelics regarding mental health has lead to an increasing number of potential therapeutic applications. In some parts of the world, psychedelics are undergoing clinical trials and active applications, offering much needed potential treatment for psychiatric and psychological disorders. Some studies have shown the therapeutic applications with a high level of success in treatments for addiction, depression and PTSD, to name a few, allowing patients to face trauma from an entirely different perspective[^fn:6].

---
# Final thoughts

{{< lead >}}
“Who comes back through the Door will never be quite the same as who went out. They will be wiser but less sure, happier but less self-satisfied, humbler in acknowledging their ignorance yet better equipped to understand the relationship of words to things, of systematic reasoning to the unfathomable mystery which it tries, forever vainly, to comprehend”

― Aldous Huxley, The Doors of Perception
{{< /lead >}}

The place we have in our world is ultimately defined by our relationship with it, with ourselves. Familiarity often leads to indifference and gaining perspective can only bring about a better appreciation and understanding of our nature and the world around us. Ultimately, life doesn't need to have a meaning other than to be experienced, and consciousness is perhaps a way of the world to experience and appreciate itself. 

[^fn:1]: [Homological scaffolds of brain functional networks](https://royalsocietypublishing.org/doi/10.1098/rsif.2014.0873)

[^fn:2]: [Biosynthesis and Extracellular Concentrations of N,N-dimethyltryptamine (DMT) in Mammalian Brain](https://www.nature.com/articles/s41598-019-45812-w)

[^fn:3]: See The Spirit Molecule by Rick Strassman

[^fn:4]: [Geometric visual hallucinations, Euclidean symmetry and the functional architecture of striate cortex](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1088430/pdf/TB010299.pdf)

[^fn:5]: [Self unbound: ego dissolution in psychedelic experience](https://academic.oup.com/nc/article/2017/1/nix016/3916730)

[^fn:6]: [The Johns Hopkins Center for Psychedelic & Consciousness Research](https://hopkinspsychedelic.org/)

<p style="text-align: center; font-size: 50px";>
{{<icon "comment">}}
</p>
{{< comments any-colour-u-like >}}
