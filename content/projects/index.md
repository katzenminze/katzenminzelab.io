---
title: "Projects"
date: 2021-04-13T17:52:38-03:00
draft: false
---
Here I'll be adding some of the projects and courses I've been working on.

Courses: 
## Coursera
### Python for Everybody Specialization
- 1/5: Getting Started with Python [certification](https://coursera.org/share/beb734431a5ea13d9672313b02f92110)
- 2/5: Python Data Structures [certification](https://coursera.org/share/95b42ad9e5f455093a2c6a0b8ba66258)
- 3/5: Using Python to Access Web Data [certification](https://coursera.org/share/693ee0b55759016a9e18d92bb7aa8c44)
- 4/5: Using Data Bases with Python **ongoing** 
- 5/5: Capstone **ongoing**

## FreeCodeCamp
One of my goals is to complete the entire curriculum offered by FreeCodeCamp. Here I'll be posting my progress through each course.

Note: Most of the content of the following projects is a meme revolving around my cat Missie. Nonetheless, the code itself is what matters and what was evaluated.
### Responsive Web Design Certification
- [Tribute Page](/projects/freecodecamp-projects/ResponsiveWebDesign/TributePage/)
- [Survey Form](/projects/freecodecamp-projects/ResponsiveWebDesign/SurveyForm/)
- [Product Landing Page](/projects/freecodecamp-projects/ResponsiveWebDesign/ProductLandingPage/)
- [Documentation Page](/projects/freecodecamp-projects/ResponsiveWebDesign/DocumentationPage/)
- [Portfolio](/projects/freecodecamp-projects/ResponsiveWebDesign/PersonalPortfolioPage/)

My certification [here](https://www.freecodecamp.org/certification/katzenminze/responsive-web-design)

### JavaScript Algorithms and Data Structures
Certification [here](https://www.freecodecamp.org/certification/katzenminze/javascript-algorithms-and-data-structures)




